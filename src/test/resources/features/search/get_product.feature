Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  Scenario: Searching for Apple returns apple related grocery
    Given he searches for "apple"
    When a response is received
    Then he sees the results displayed contains any of:
      | apple |
      | appel |
      | jumbo |
      | stuk  |
    And returns the status code 200

  Scenario: Searching for Mango returns mango related grocery
    Given he searches for "mango"
    When a response is received
    Then he sees the results displayed contains any of:
      | mango |
      | jumbo |
      | appel |
      | stuk  |
      | vruchtendrank |
    And returns the status code 200

  Scenario: Searching for nonexistent item should return an error and status code 404
    Given he searches for "car"
    When a response is received
    Then he does not see the results
    And returns the status code 404

