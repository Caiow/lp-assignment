package starter.stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import model.Product;
import net.serenitybdd.rest.SerenityRest;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;

public class SearchStepDefinitions extends BaseTest {

    @Given("he searches for {string}")
    public void he_searches_for(String item) {
            SerenityRest.given().get(BASE_URI + item);
    }

    @When("a response is received")
    public void a_response_is_received() {
        restAssuredThat(response -> response.extract().statusCode());
    }

    @Then("he sees the results displayed contains any of:")
    public void he_sees_the_results_displayed_contains_any_of(DataTable itemTable) {
        Product[] products = lastResponse().getBody().as(Product[].class);
        String[] items = itemTable.asList(String.class).toArray(new String[itemTable.height()]);
        for (Product product : products) {
            assertThat(product.getTitle().toLowerCase()).containsAnyOf(items);
        }
    }

    @And("returns the status code {int}")
    public void returns_the_status_code(Integer statusCode) {
        restAssuredThat(response -> response.statusCode(statusCode));
    }

    @Then("he does not see the results")
    public void he_does_not_see_the_results() {
        restAssuredThat(response -> response.body("detail.error", is(true)));
    }
}
