package model;

public class Product {
    String provider;
    String title;
    String url;
    String brand;
    Double price;
    String unit;
    String isPromo;
    String promoDetails;
    String image;

    public Product(String provider, String title, String url, String brand, Double price, String unit, String isPromo, String promoDetails, String image) {
        this.provider = provider;
        this.title = title;
        this.url = url;
        this.brand = brand;
        this.price = price;
        this.unit = unit;
        this.isPromo = isPromo;
        this.promoDetails = promoDetails;
        this.image = image;
    }

    public Product() {
    }

    public String getProvider() {
        return provider;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getBrand() {
        return brand;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getIsPromo() {
        return isPromo;
    }

    public String getPromoDetails() {
        return promoDetails;
    }

    public String getImage() {
        return image;
    }
}
