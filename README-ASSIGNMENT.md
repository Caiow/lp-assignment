# LeasePlan BE QA Auto Test Assignment
## Installing and running tests locally
Tests were written in Java, using Serenity and Cucumber. <br>
For running the scripts, you must have installed **JDK 8+** and **Maven**. <br>
<br>
Once you have it installed, you can run inside the project directory the command: 
`
mvn clean verify
`

This command is going to clear the *target* folder, in which is going to save the execution reports, and also run the tests located and on the path defined on the ``TestRunner`` class.
<br>
## Creating a Test Case
### A sample scenario
```Gherkin
Scenario: Searching for nonexistent item should return an error and status code 404
    When he searches for "car"
    Then he does not see the results
      And returns the status code 404
```

For writing new scenarios, you can follow the folder structure presented previously in order to keep a good pattern.
<br><br>
The `FeatureFile` is used to create the test scenario and test cases you want. 
<br><br>
The `stepdefinitions` folder contains the java classes with the *Steps Definitions*, you create a new class for handling your steps, or use an existent one if you're creating a test for an already existing Feature.

### A sample Step Definition
```Java
@Then("he sees the results displayed contains any of:")
    public void he_sees_the_results_displayed_contains_any_of(DataTable itemTable) {
        Product[] products = lastResponse().getBody().as(Product[].class);
        String[] items = itemTable.asList(String.class).toArray(new String[itemTable.height()]);
        for (Product product : products) {
            assertThat(product.getTitle().toLowerCase()).containsAnyOf(items);
        }
    }
```

## What was changed and why

Almost everything had a touch from my part, since I tried to make everything the clear as possible.
<br><br>
Created the `model` folder and the `Product.java` class for handling objects  coming from requests, so on other tests we could handle that in a more simple way.
<br>
Every object inside the JSON response is stored into a new `Product` object array, so that way it's also possible to validate every response from the request separately.
<br><br>
Also changed the `SearchStepDefinition` class, putting the step definition functions on `snake_case`, instead of the *camelCase* that was being used before, and not on all functions, so I also established a pattern.
<br><br>
Also created a single and more general step for searching and validating products results:
```Java
@Given("he searches for {string}")
public void he_searches_for(String item) {
```
and the validation method, that receives a DataTable of expected words to be on the product title:
```Java
@Then("he sees the results displayed contains any of:")
public void he_sees_the_results_displayed_contains_any_of(DataTable itemTable) {
```
The DataTable values are converted to a list of words, that are later checked if any of these words are present on the Product title.